﻿namespace SmartBadmintonTrainingSystem
{
    partial class Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Test));
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_test = new System.Windows.Forms.Button();
            this.choose_order = new System.Windows.Forms.RadioButton();
            this.random_order = new System.Windows.Forms.RadioButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.test_12 = new System.Windows.Forms.RadioButton();
            this.test_24 = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Picture_Status = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.goalSwingAmount = new System.Windows.Forms.Label();
            this.currentSwingAmount = new System.Windows.Forms.Label();
            this.center = new System.Windows.Forms.PictureBox();
            this.p8 = new System.Windows.Forms.PictureBox();
            this.p7 = new System.Windows.Forms.PictureBox();
            this.p6 = new System.Windows.Forms.PictureBox();
            this.p5 = new System.Windows.Forms.PictureBox();
            this.p4 = new System.Windows.Forms.PictureBox();
            this.p3 = new System.Windows.Forms.PictureBox();
            this.p2 = new System.Windows.Forms.PictureBox();
            this.p1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_Status)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.center)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(174, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 25);
            this.label2.TabIndex = 51;
            this.label2.Text = "컨트롤러 연결:X";
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox1.Font = new System.Drawing.Font("굴림", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 32;
            this.comboBox1.Location = new System.Drawing.Point(10, 11);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(320, 40);
            this.comboBox1.TabIndex = 56;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(14, 510);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(307, 120);
            this.button2.TabIndex = 58;
            this.button2.Text = "테스트 시작";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 24F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(76, 68);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(180, 60);
            this.button1.TabIndex = 52;
            this.button1.Text = "연결시도";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(14, 427);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(307, 78);
            this.button3.TabIndex = 59;
            this.button3.Text = "테스트 중지";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_test
            // 
            this.btn_test.BackColor = System.Drawing.Color.Transparent;
            this.btn_test.FlatAppearance.BorderSize = 0;
            this.btn_test.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.btn_test.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_test.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.btn_test.ForeColor = System.Drawing.Color.White;
            this.btn_test.Location = new System.Drawing.Point(14, 303);
            this.btn_test.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_test.Name = "btn_test";
            this.btn_test.Size = new System.Drawing.Size(164, 120);
            this.btn_test.TabIndex = 70;
            this.btn_test.Text = "순서 설정";
            this.btn_test.UseVisualStyleBackColor = false;
            this.btn_test.Click += new System.EventHandler(this.btn_test_Click_1);
            // 
            // choose_order
            // 
            this.choose_order.AutoSize = true;
            this.choose_order.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.choose_order.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.choose_order.ForeColor = System.Drawing.Color.White;
            this.choose_order.Location = new System.Drawing.Point(4, 72);
            this.choose_order.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.choose_order.Name = "choose_order";
            this.choose_order.Size = new System.Drawing.Size(110, 29);
            this.choose_order.TabIndex = 72;
            this.choose_order.TabStop = true;
            this.choose_order.Text = "선택 순서";
            this.choose_order.UseVisualStyleBackColor = true;
            this.choose_order.CheckedChanged += new System.EventHandler(this.choose_order_CheckedChanged);
            // 
            // random_order
            // 
            this.random_order.AutoSize = true;
            this.random_order.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.random_order.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.random_order.ForeColor = System.Drawing.Color.White;
            this.random_order.Location = new System.Drawing.Point(4, 26);
            this.random_order.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.random_order.Name = "random_order";
            this.random_order.Size = new System.Drawing.Size(110, 29);
            this.random_order.TabIndex = 71;
            this.random_order.TabStop = true;
            this.random_order.Text = "랜덤 순서";
            this.random_order.UseVisualStyleBackColor = true;
            this.random_order.CheckedChanged += new System.EventHandler(this.random_order_CheckedChanged);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 15;
            this.listBox1.Location = new System.Drawing.Point(1336, 49);
            this.listBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(557, 994);
            this.listBox1.TabIndex = 74;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.random_order);
            this.panel2.Controls.Add(this.choose_order);
            this.panel2.Location = new System.Drawing.Point(180, 303);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(140, 120);
            this.panel2.TabIndex = 75;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.test_12);
            this.panel4.Controls.Add(this.test_24);
            this.panel4.Location = new System.Drawing.Point(14, 179);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(306, 120);
            this.panel4.TabIndex = 76;
            // 
            // test_12
            // 
            this.test_12.AutoSize = true;
            this.test_12.BackColor = System.Drawing.Color.Transparent;
            this.test_12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.test_12.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.test_12.ForeColor = System.Drawing.Color.White;
            this.test_12.Location = new System.Drawing.Point(4, 26);
            this.test_12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.test_12.Name = "test_12";
            this.test_12.Size = new System.Drawing.Size(130, 29);
            this.test_12.TabIndex = 71;
            this.test_12.TabStop = true;
            this.test_12.Text = "12회 테스트";
            this.test_12.UseVisualStyleBackColor = false;
            this.test_12.CheckedChanged += new System.EventHandler(this.test_12_CheckedChanged);
            // 
            // test_24
            // 
            this.test_24.AutoSize = true;
            this.test_24.BackColor = System.Drawing.Color.Transparent;
            this.test_24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.test_24.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.test_24.ForeColor = System.Drawing.Color.White;
            this.test_24.Location = new System.Drawing.Point(4, 72);
            this.test_24.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.test_24.Name = "test_24";
            this.test_24.Size = new System.Drawing.Size(130, 29);
            this.test_24.TabIndex = 72;
            this.test_24.TabStop = true;
            this.test_24.Text = "24회 테스트";
            this.test_24.UseVisualStyleBackColor = false;
            this.test_24.CheckedChanged += new System.EventHandler(this.test_24_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.panel5.Controls.Add(this.panel2);
            this.panel5.Controls.Add(this.panel4);
            this.panel5.Controls.Add(this.btn_test);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Controls.Add(this.button3);
            this.panel5.Controls.Add(this.comboBox1);
            this.panel5.Controls.Add(this.button2);
            this.panel5.Controls.Add(this.button1);
            this.panel5.Controls.Add(this.Picture_Status);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Location = new System.Drawing.Point(0, 46);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(339, 1034);
            this.panel5.TabIndex = 77;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SmartBadmintonTrainingSystem.Properties.Resources.refresh;
            this.pictureBox1.Location = new System.Drawing.Point(256, 64);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Padding = new System.Windows.Forms.Padding(16);
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 74;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.button4_Click);
            // 
            // Picture_Status
            // 
            this.Picture_Status.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Picture_Status.Image = global::SmartBadmintonTrainingSystem.Properties.Resources.signal_red;
            this.Picture_Status.Location = new System.Drawing.Point(0, 68);
            this.Picture_Status.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Picture_Status.Name = "Picture_Status";
            this.Picture_Status.Padding = new System.Windows.Forms.Padding(16, 14, 16, 14);
            this.Picture_Status.Size = new System.Drawing.Size(70, 64);
            this.Picture_Status.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Picture_Status.TabIndex = 53;
            this.Picture_Status.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(253)))));
            this.panel1.BackgroundImage = global::SmartBadmintonTrainingSystem.Properties.Resources.제목_없음_1_사본;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.goalSwingAmount);
            this.panel1.Controls.Add(this.currentSwingAmount);
            this.panel1.Controls.Add(this.center);
            this.panel1.Controls.Add(this.p8);
            this.panel1.Controls.Add(this.p7);
            this.panel1.Controls.Add(this.p6);
            this.panel1.Controls.Add(this.p5);
            this.panel1.Controls.Add(this.p4);
            this.panel1.Controls.Add(this.p3);
            this.panel1.Controls.Add(this.p2);
            this.panel1.Controls.Add(this.p1);
            this.panel1.Location = new System.Drawing.Point(339, 46);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(994, 994);
            this.panel1.TabIndex = 54;
            // 
            // goalSwingAmount
            // 
            this.goalSwingAmount.AutoSize = true;
            this.goalSwingAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.goalSwingAmount.Font = new System.Drawing.Font("Segoe UI", 45F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goalSwingAmount.ForeColor = System.Drawing.Color.White;
            this.goalSwingAmount.Location = new System.Drawing.Point(506, 579);
            this.goalSwingAmount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.goalSwingAmount.Name = "goalSwingAmount";
            this.goalSwingAmount.Size = new System.Drawing.Size(99, 81);
            this.goalSwingAmount.TabIndex = 74;
            this.goalSwingAmount.Text = "00";
            // 
            // currentSwingAmount
            // 
            this.currentSwingAmount.AutoSize = true;
            this.currentSwingAmount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.currentSwingAmount.Font = new System.Drawing.Font("Segoe UI", 45F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentSwingAmount.ForeColor = System.Drawing.Color.White;
            this.currentSwingAmount.Location = new System.Drawing.Point(403, 579);
            this.currentSwingAmount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.currentSwingAmount.Name = "currentSwingAmount";
            this.currentSwingAmount.Size = new System.Drawing.Size(99, 81);
            this.currentSwingAmount.TabIndex = 73;
            this.currentSwingAmount.Text = "00";
            // 
            // center
            // 
            this.center.BackColor = System.Drawing.Color.Transparent;
            this.center.Image = ((System.Drawing.Image)(resources.GetObject("center.Image")));
            this.center.Location = new System.Drawing.Point(443, 393);
            this.center.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.center.Name = "center";
            this.center.Size = new System.Drawing.Size(120, 120);
            this.center.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.center.TabIndex = 71;
            this.center.TabStop = false;
            // 
            // p8
            // 
            this.p8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.p8.BackColor = System.Drawing.Color.Transparent;
            this.p8.Image = ((System.Drawing.Image)(resources.GetObject("p8.Image")));
            this.p8.Location = new System.Drawing.Point(754, 714);
            this.p8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(200, 200);
            this.p8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p8.TabIndex = 62;
            this.p8.TabStop = false;
            // 
            // p7
            // 
            this.p7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.p7.BackColor = System.Drawing.Color.Transparent;
            this.p7.Image = ((System.Drawing.Image)(resources.GetObject("p7.Image")));
            this.p7.Location = new System.Drawing.Point(405, 714);
            this.p7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(200, 200);
            this.p7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p7.TabIndex = 61;
            this.p7.TabStop = false;
            // 
            // p6
            // 
            this.p6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.p6.BackColor = System.Drawing.Color.Transparent;
            this.p6.Image = ((System.Drawing.Image)(resources.GetObject("p6.Image")));
            this.p6.Location = new System.Drawing.Point(55, 714);
            this.p6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(200, 200);
            this.p6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p6.TabIndex = 60;
            this.p6.TabStop = false;
            // 
            // p5
            // 
            this.p5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.p5.BackColor = System.Drawing.Color.Transparent;
            this.p5.Image = ((System.Drawing.Image)(resources.GetObject("p5.Image")));
            this.p5.Location = new System.Drawing.Point(754, 352);
            this.p5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(200, 200);
            this.p5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p5.TabIndex = 59;
            this.p5.TabStop = false;
            // 
            // p4
            // 
            this.p4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.p4.BackColor = System.Drawing.Color.Transparent;
            this.p4.Image = ((System.Drawing.Image)(resources.GetObject("p4.Image")));
            this.p4.Location = new System.Drawing.Point(55, 358);
            this.p4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(200, 200);
            this.p4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p4.TabIndex = 57;
            this.p4.TabStop = false;
            // 
            // p3
            // 
            this.p3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.p3.BackColor = System.Drawing.Color.Transparent;
            this.p3.Image = ((System.Drawing.Image)(resources.GetObject("p3.Image")));
            this.p3.Location = new System.Drawing.Point(754, 22);
            this.p3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(200, 200);
            this.p3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p3.TabIndex = 56;
            this.p3.TabStop = false;
            this.p3.Click += new System.EventHandler(this.p3_Click);
            // 
            // p2
            // 
            this.p2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.p2.BackColor = System.Drawing.Color.Transparent;
            this.p2.Image = ((System.Drawing.Image)(resources.GetObject("p2.Image")));
            this.p2.Location = new System.Drawing.Point(405, 22);
            this.p2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(200, 200);
            this.p2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p2.TabIndex = 55;
            this.p2.TabStop = false;
            // 
            // p1
            // 
            this.p1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.p1.BackColor = System.Drawing.Color.Transparent;
            this.p1.Image = ((System.Drawing.Image)(resources.GetObject("p1.Image")));
            this.p1.Location = new System.Drawing.Point(55, 22);
            this.p1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(200, 200);
            this.p1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p1.TabIndex = 54;
            this.p1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Tomato;
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(0, -2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1920, 48);
            this.panel3.TabIndex = 78;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::SmartBadmintonTrainingSystem.Properties.Resources.close_button;
            this.pictureBox2.Location = new System.Drawing.Point(1876, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Test";
            // 
            // Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Test";
            this.Text = "Test";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Test_FormClosed);
            this.Load += new System.EventHandler(this.Test_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_Status)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.center)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
       
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox Picture_Status;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox p8;
        private System.Windows.Forms.PictureBox p7;
        private System.Windows.Forms.PictureBox p6;
        private System.Windows.Forms.PictureBox p5;
        private System.Windows.Forms.PictureBox p4;
        private System.Windows.Forms.PictureBox p3;
        private System.Windows.Forms.PictureBox p2;
        private System.Windows.Forms.PictureBox p1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btn_test;
        private System.Windows.Forms.RadioButton choose_order;
        private System.Windows.Forms.RadioButton random_order;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.PictureBox center;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton test_12;
        private System.Windows.Forms.RadioButton test_24;
        private System.Windows.Forms.Label goalSwingAmount;
        private System.Windows.Forms.Label currentSwingAmount;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}
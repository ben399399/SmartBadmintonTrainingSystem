﻿namespace SmartBadmintonTrainingSystem
{
    partial class CustomProgramForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomProgramForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.confirmButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.TrainingSet = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.progNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.one_count = new System.Windows.Forms.TextBox();
            this.two_count = new System.Windows.Forms.TextBox();
            this.three_count = new System.Windows.Forms.TextBox();
            this.five_count = new System.Windows.Forms.TextBox();
            this.four_count = new System.Windows.Forms.TextBox();
            this.six_count = new System.Windows.Forms.TextBox();
            this.seven_count = new System.Windows.Forms.TextBox();
            this.eight_count = new System.Windows.Forms.TextBox();
            this.eight_up = new System.Windows.Forms.PictureBox();
            this.eight_down = new System.Windows.Forms.PictureBox();
            this.seven_up = new System.Windows.Forms.PictureBox();
            this.seven_down = new System.Windows.Forms.PictureBox();
            this.six_up = new System.Windows.Forms.PictureBox();
            this.six_down = new System.Windows.Forms.PictureBox();
            this.five_up = new System.Windows.Forms.PictureBox();
            this.five_down = new System.Windows.Forms.PictureBox();
            this.four_up = new System.Windows.Forms.PictureBox();
            this.four_down = new System.Windows.Forms.PictureBox();
            this.three_up = new System.Windows.Forms.PictureBox();
            this.three_down = new System.Windows.Forms.PictureBox();
            this.two_up = new System.Windows.Forms.PictureBox();
            this.two_down = new System.Windows.Forms.PictureBox();
            this.one_up = new System.Windows.Forms.PictureBox();
            this.one_down = new System.Windows.Forms.PictureBox();
            this.closeButton = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eight_up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eight_down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seven_up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seven_down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.six_up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.six_down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.five_up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.five_down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.four_up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.four_down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.three_up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.three_down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.two_up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.two_down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.one_up)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.one_down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeButton)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Tomato;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(971, 72);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(23, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "사용자 지정 프로그램 설정";
            // 
            // confirmButton
            // 
            this.confirmButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.confirmButton.FlatAppearance.BorderSize = 0;
            this.confirmButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirmButton.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.confirmButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.confirmButton.Location = new System.Drawing.Point(844, 630);
            this.confirmButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(104, 54);
            this.confirmButton.TabIndex = 3;
            this.confirmButton.Text = "확인";
            this.confirmButton.UseVisualStyleBackColor = false;
            this.confirmButton.Click += new System.EventHandler(this.confirmButton_Click);
            this.confirmButton.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.confirmButton.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(270, 228);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "1번 기둥";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(533, 228);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "2번 기둥";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(796, 228);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "3번 기둥";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.panel2.Controls.Add(this.button1);
            this.panel2.Location = new System.Drawing.Point(0, 72);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(241, 636);
            this.panel2.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(241, 62);
            this.button1.TabIndex = 0;
            this.button1.Text = "랜덤";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(796, 374);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 18);
            this.label4.TabIndex = 25;
            this.label4.Text = "5번 기둥";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(270, 374);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 18);
            this.label10.TabIndex = 17;
            this.label10.Text = "4번 기둥";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(796, 519);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 18);
            this.label12.TabIndex = 37;
            this.label12.Text = "8번 기둥";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(533, 519);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 18);
            this.label14.TabIndex = 33;
            this.label14.Text = "7번 기둥";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(270, 519);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 18);
            this.label16.TabIndex = 29;
            this.label16.Text = "6번 기둥";
            // 
            // TrainingSet
            // 
            this.TrainingSet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.TrainingSet.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TrainingSet.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.TrainingSet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.TrainingSet.Location = new System.Drawing.Point(273, 646);
            this.TrainingSet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TrainingSet.Name = "TrainingSet";
            this.TrainingSet.Size = new System.Drawing.Size(480, 28);
            this.TrainingSet.TabIndex = 41;
            this.TrainingSet.Enter += new System.EventHandler(this.textBox1_Enter);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.panel3.Location = new System.Drawing.Point(273, 674);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(480, 3);
            this.panel3.TabIndex = 68;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Gainsboro;
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.progNameTextBox);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Location = new System.Drawing.Point(241, 72);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(730, 74);
            this.panel4.TabIndex = 69;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.panel5.Location = new System.Drawing.Point(174, 50);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(509, 3);
            this.panel5.TabIndex = 69;
            // 
            // progNameTextBox
            // 
            this.progNameTextBox.BackColor = System.Drawing.Color.Gainsboro;
            this.progNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.progNameTextBox.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.progNameTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.progNameTextBox.Location = new System.Drawing.Point(176, 22);
            this.progNameTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.progNameTextBox.MaxLength = 10;
            this.progNameTextBox.Name = "progNameTextBox";
            this.progNameTextBox.Size = new System.Drawing.Size(507, 32);
            this.progNameTextBox.TabIndex = 70;
            this.progNameTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(27, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(139, 28);
            this.label3.TabIndex = 0;
            this.label3.Text = "프로그램 이름";
            // 
            // one_count
            // 
            this.one_count.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.one_count.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.one_count.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.one_count.ForeColor = System.Drawing.Color.Tomato;
            this.one_count.Location = new System.Drawing.Point(343, 202);
            this.one_count.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.one_count.MaxLength = 2;
            this.one_count.Name = "one_count";
            this.one_count.Size = new System.Drawing.Size(60, 54);
            this.one_count.TabIndex = 70;
            this.one_count.Text = "0";
            this.one_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.one_count.TextChanged += new System.EventHandler(this.count_TextChanged);
            this.one_count.Leave += new System.EventHandler(this.count_Leave);
            // 
            // two_count
            // 
            this.two_count.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.two_count.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.two_count.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.two_count.ForeColor = System.Drawing.Color.Tomato;
            this.two_count.Location = new System.Drawing.Point(606, 202);
            this.two_count.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.two_count.MaxLength = 2;
            this.two_count.Name = "two_count";
            this.two_count.Size = new System.Drawing.Size(60, 54);
            this.two_count.TabIndex = 71;
            this.two_count.Text = "0";
            this.two_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.two_count.TextChanged += new System.EventHandler(this.count_TextChanged);
            this.two_count.Leave += new System.EventHandler(this.count_Leave);
            // 
            // three_count
            // 
            this.three_count.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.three_count.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.three_count.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.three_count.ForeColor = System.Drawing.Color.Tomato;
            this.three_count.Location = new System.Drawing.Point(869, 202);
            this.three_count.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.three_count.MaxLength = 2;
            this.three_count.Name = "three_count";
            this.three_count.Size = new System.Drawing.Size(60, 54);
            this.three_count.TabIndex = 72;
            this.three_count.Text = "0";
            this.three_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.three_count.TextChanged += new System.EventHandler(this.count_TextChanged);
            this.three_count.Leave += new System.EventHandler(this.count_Leave);
            // 
            // five_count
            // 
            this.five_count.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.five_count.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.five_count.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.five_count.ForeColor = System.Drawing.Color.Tomato;
            this.five_count.Location = new System.Drawing.Point(869, 348);
            this.five_count.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.five_count.MaxLength = 2;
            this.five_count.Name = "five_count";
            this.five_count.Size = new System.Drawing.Size(60, 54);
            this.five_count.TabIndex = 73;
            this.five_count.Text = "0";
            this.five_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.five_count.TextChanged += new System.EventHandler(this.count_TextChanged);
            this.five_count.Leave += new System.EventHandler(this.count_Leave);
            // 
            // four_count
            // 
            this.four_count.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.four_count.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.four_count.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.four_count.ForeColor = System.Drawing.Color.Tomato;
            this.four_count.Location = new System.Drawing.Point(343, 348);
            this.four_count.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.four_count.MaxLength = 2;
            this.four_count.Name = "four_count";
            this.four_count.Size = new System.Drawing.Size(60, 54);
            this.four_count.TabIndex = 74;
            this.four_count.Text = "0";
            this.four_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.four_count.TextChanged += new System.EventHandler(this.count_TextChanged);
            this.four_count.Leave += new System.EventHandler(this.count_Leave);
            // 
            // six_count
            // 
            this.six_count.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.six_count.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.six_count.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.six_count.ForeColor = System.Drawing.Color.Tomato;
            this.six_count.Location = new System.Drawing.Point(343, 494);
            this.six_count.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.six_count.MaxLength = 2;
            this.six_count.Name = "six_count";
            this.six_count.Size = new System.Drawing.Size(60, 54);
            this.six_count.TabIndex = 75;
            this.six_count.Text = "0";
            this.six_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.six_count.TextChanged += new System.EventHandler(this.count_TextChanged);
            this.six_count.Leave += new System.EventHandler(this.count_Leave);
            // 
            // seven_count
            // 
            this.seven_count.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.seven_count.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.seven_count.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.seven_count.ForeColor = System.Drawing.Color.Tomato;
            this.seven_count.Location = new System.Drawing.Point(606, 494);
            this.seven_count.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.seven_count.MaxLength = 2;
            this.seven_count.Name = "seven_count";
            this.seven_count.Size = new System.Drawing.Size(60, 54);
            this.seven_count.TabIndex = 76;
            this.seven_count.Text = "0";
            this.seven_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.seven_count.TextChanged += new System.EventHandler(this.count_TextChanged);
            this.seven_count.Leave += new System.EventHandler(this.count_Leave);
            // 
            // eight_count
            // 
            this.eight_count.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.eight_count.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.eight_count.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.eight_count.ForeColor = System.Drawing.Color.Tomato;
            this.eight_count.Location = new System.Drawing.Point(869, 494);
            this.eight_count.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.eight_count.MaxLength = 2;
            this.eight_count.Name = "eight_count";
            this.eight_count.Size = new System.Drawing.Size(60, 54);
            this.eight_count.TabIndex = 77;
            this.eight_count.Text = "0";
            this.eight_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.eight_count.TextChanged += new System.EventHandler(this.count_TextChanged);
            this.eight_count.Leave += new System.EventHandler(this.count_Leave);
            // 
            // eight_up
            // 
            this.eight_up.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.eight_up.Image = ((System.Drawing.Image)(resources.GetObject("eight_up.Image")));
            this.eight_up.Location = new System.Drawing.Point(887, 468);
            this.eight_up.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.eight_up.Name = "eight_up";
            this.eight_up.Size = new System.Drawing.Size(23, 24);
            this.eight_up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.eight_up.TabIndex = 40;
            this.eight_up.TabStop = false;
            this.eight_up.Click += new System.EventHandler(this.up_Click);
            this.eight_up.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.eight_up.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // eight_down
            // 
            this.eight_down.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.eight_down.Image = ((System.Drawing.Image)(resources.GetObject("eight_down.Image")));
            this.eight_down.Location = new System.Drawing.Point(887, 555);
            this.eight_down.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.eight_down.Name = "eight_down";
            this.eight_down.Size = new System.Drawing.Size(23, 24);
            this.eight_down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.eight_down.TabIndex = 39;
            this.eight_down.TabStop = false;
            this.eight_down.Click += new System.EventHandler(this.down_click);
            this.eight_down.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.eight_down.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // seven_up
            // 
            this.seven_up.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.seven_up.Image = ((System.Drawing.Image)(resources.GetObject("seven_up.Image")));
            this.seven_up.Location = new System.Drawing.Point(624, 468);
            this.seven_up.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.seven_up.Name = "seven_up";
            this.seven_up.Size = new System.Drawing.Size(23, 24);
            this.seven_up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.seven_up.TabIndex = 36;
            this.seven_up.TabStop = false;
            this.seven_up.Click += new System.EventHandler(this.up_Click);
            this.seven_up.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.seven_up.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // seven_down
            // 
            this.seven_down.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.seven_down.Image = ((System.Drawing.Image)(resources.GetObject("seven_down.Image")));
            this.seven_down.Location = new System.Drawing.Point(624, 555);
            this.seven_down.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.seven_down.Name = "seven_down";
            this.seven_down.Size = new System.Drawing.Size(23, 24);
            this.seven_down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.seven_down.TabIndex = 35;
            this.seven_down.TabStop = false;
            this.seven_down.Click += new System.EventHandler(this.down_click);
            this.seven_down.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.seven_down.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // six_up
            // 
            this.six_up.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.six_up.Image = ((System.Drawing.Image)(resources.GetObject("six_up.Image")));
            this.six_up.Location = new System.Drawing.Point(361, 468);
            this.six_up.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.six_up.Name = "six_up";
            this.six_up.Size = new System.Drawing.Size(23, 24);
            this.six_up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.six_up.TabIndex = 32;
            this.six_up.TabStop = false;
            this.six_up.Click += new System.EventHandler(this.up_Click);
            this.six_up.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.six_up.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // six_down
            // 
            this.six_down.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.six_down.Image = ((System.Drawing.Image)(resources.GetObject("six_down.Image")));
            this.six_down.Location = new System.Drawing.Point(361, 555);
            this.six_down.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.six_down.Name = "six_down";
            this.six_down.Size = new System.Drawing.Size(23, 24);
            this.six_down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.six_down.TabIndex = 31;
            this.six_down.TabStop = false;
            this.six_down.Click += new System.EventHandler(this.down_click);
            this.six_down.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.six_down.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // five_up
            // 
            this.five_up.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.five_up.Image = ((System.Drawing.Image)(resources.GetObject("five_up.Image")));
            this.five_up.Location = new System.Drawing.Point(887, 322);
            this.five_up.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.five_up.Name = "five_up";
            this.five_up.Size = new System.Drawing.Size(23, 24);
            this.five_up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.five_up.TabIndex = 28;
            this.five_up.TabStop = false;
            this.five_up.Click += new System.EventHandler(this.up_Click);
            this.five_up.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.five_up.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // five_down
            // 
            this.five_down.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.five_down.Image = ((System.Drawing.Image)(resources.GetObject("five_down.Image")));
            this.five_down.Location = new System.Drawing.Point(887, 414);
            this.five_down.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.five_down.Name = "five_down";
            this.five_down.Size = new System.Drawing.Size(23, 24);
            this.five_down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.five_down.TabIndex = 27;
            this.five_down.TabStop = false;
            this.five_down.Click += new System.EventHandler(this.down_click);
            this.five_down.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.five_down.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // four_up
            // 
            this.four_up.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.four_up.Image = ((System.Drawing.Image)(resources.GetObject("four_up.Image")));
            this.four_up.Location = new System.Drawing.Point(361, 322);
            this.four_up.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.four_up.Name = "four_up";
            this.four_up.Size = new System.Drawing.Size(23, 24);
            this.four_up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.four_up.TabIndex = 20;
            this.four_up.TabStop = false;
            this.four_up.Click += new System.EventHandler(this.up_Click);
            this.four_up.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.four_up.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // four_down
            // 
            this.four_down.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.four_down.Image = ((System.Drawing.Image)(resources.GetObject("four_down.Image")));
            this.four_down.Location = new System.Drawing.Point(361, 414);
            this.four_down.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.four_down.Name = "four_down";
            this.four_down.Size = new System.Drawing.Size(23, 24);
            this.four_down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.four_down.TabIndex = 19;
            this.four_down.TabStop = false;
            this.four_down.Click += new System.EventHandler(this.down_click);
            this.four_down.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.four_down.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // three_up
            // 
            this.three_up.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.three_up.Image = ((System.Drawing.Image)(resources.GetObject("three_up.Image")));
            this.three_up.Location = new System.Drawing.Point(887, 177);
            this.three_up.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.three_up.Name = "three_up";
            this.three_up.Size = new System.Drawing.Size(23, 24);
            this.three_up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.three_up.TabIndex = 15;
            this.three_up.TabStop = false;
            this.three_up.Click += new System.EventHandler(this.up_Click);
            this.three_up.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.three_up.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // three_down
            // 
            this.three_down.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.three_down.Image = ((System.Drawing.Image)(resources.GetObject("three_down.Image")));
            this.three_down.Location = new System.Drawing.Point(887, 264);
            this.three_down.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.three_down.Name = "three_down";
            this.three_down.Size = new System.Drawing.Size(23, 24);
            this.three_down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.three_down.TabIndex = 14;
            this.three_down.TabStop = false;
            this.three_down.Click += new System.EventHandler(this.down_click);
            this.three_down.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.three_down.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // two_up
            // 
            this.two_up.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.two_up.Image = ((System.Drawing.Image)(resources.GetObject("two_up.Image")));
            this.two_up.Location = new System.Drawing.Point(624, 177);
            this.two_up.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.two_up.Name = "two_up";
            this.two_up.Size = new System.Drawing.Size(23, 24);
            this.two_up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.two_up.TabIndex = 11;
            this.two_up.TabStop = false;
            this.two_up.Click += new System.EventHandler(this.up_Click);
            this.two_up.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.two_up.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // two_down
            // 
            this.two_down.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.two_down.Image = ((System.Drawing.Image)(resources.GetObject("two_down.Image")));
            this.two_down.Location = new System.Drawing.Point(624, 264);
            this.two_down.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.two_down.Name = "two_down";
            this.two_down.Size = new System.Drawing.Size(23, 24);
            this.two_down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.two_down.TabIndex = 10;
            this.two_down.TabStop = false;
            this.two_down.Click += new System.EventHandler(this.down_click);
            this.two_down.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.two_down.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // one_up
            // 
            this.one_up.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.one_up.Image = ((System.Drawing.Image)(resources.GetObject("one_up.Image")));
            this.one_up.Location = new System.Drawing.Point(361, 177);
            this.one_up.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.one_up.Name = "one_up";
            this.one_up.Size = new System.Drawing.Size(23, 24);
            this.one_up.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.one_up.TabIndex = 7;
            this.one_up.TabStop = false;
            this.one_up.Click += new System.EventHandler(this.up_Click);
            // 
            // one_down
            // 
            this.one_down.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.one_down.Image = ((System.Drawing.Image)(resources.GetObject("one_down.Image")));
            this.one_down.Location = new System.Drawing.Point(361, 264);
            this.one_down.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.one_down.Name = "one_down";
            this.one_down.Size = new System.Drawing.Size(23, 24);
            this.one_down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.one_down.TabIndex = 6;
            this.one_down.TabStop = false;
            this.one_down.Click += new System.EventHandler(this.down_click);
            this.one_down.MouseEnter += new System.EventHandler(this.chevron_enter);
            this.one_down.MouseLeave += new System.EventHandler(this.chevron_leave);
            // 
            // closeButton
            // 
            this.closeButton.BackColor = System.Drawing.Color.Tomato;
            this.closeButton.Image = global::SmartBadmintonTrainingSystem.Properties.Resources.close_button;
            this.closeButton.Location = new System.Drawing.Point(914, 12);
            this.closeButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(46, 48);
            this.closeButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.closeButton.TabIndex = 2;
            this.closeButton.TabStop = false;
            this.closeButton.Click += new System.EventHandler(this.pictureBox1_Click);
            this.closeButton.MouseLeave += new System.EventHandler(this.closeButton_MouseLeave);
            this.closeButton.MouseHover += new System.EventHandler(this.pictureBox1_MouseEnter);
            // 
            // CustomProgramForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.ClientSize = new System.Drawing.Size(971, 706);
            this.Controls.Add(this.eight_count);
            this.Controls.Add(this.seven_count);
            this.Controls.Add(this.six_count);
            this.Controls.Add(this.four_count);
            this.Controls.Add(this.five_count);
            this.Controls.Add(this.three_count);
            this.Controls.Add(this.two_count);
            this.Controls.Add(this.one_count);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.TrainingSet);
            this.Controls.Add(this.eight_up);
            this.Controls.Add(this.eight_down);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.seven_up);
            this.Controls.Add(this.seven_down);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.six_up);
            this.Controls.Add(this.six_down);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.five_up);
            this.Controls.Add(this.five_down);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.four_up);
            this.Controls.Add(this.four_down);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.three_up);
            this.Controls.Add(this.three_down);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.two_up);
            this.Controls.Add(this.two_down);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.one_up);
            this.Controls.Add(this.one_down);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.confirmButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "CustomProgramForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "CustomProgramForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eight_up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eight_down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seven_up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seven_down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.six_up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.six_down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.five_up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.five_down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.four_up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.four_down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.three_up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.three_down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.two_up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.two_down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.one_up)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.one_down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.closeButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox closeButton;
        private System.Windows.Forms.Button confirmButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox one_down;
        private System.Windows.Forms.PictureBox one_up;
        private System.Windows.Forms.PictureBox two_up;
        private System.Windows.Forms.PictureBox two_down;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox three_up;
        private System.Windows.Forms.PictureBox three_down;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox five_up;
        private System.Windows.Forms.PictureBox five_down;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox four_up;
        private System.Windows.Forms.PictureBox four_down;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox eight_up;
        private System.Windows.Forms.PictureBox eight_down;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox seven_up;
        private System.Windows.Forms.PictureBox seven_down;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox six_up;
        private System.Windows.Forms.PictureBox six_down;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TrainingSet;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox progNameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox one_count;
        private System.Windows.Forms.TextBox two_count;
        private System.Windows.Forms.TextBox three_count;
        private System.Windows.Forms.TextBox five_count;
        private System.Windows.Forms.TextBox four_count;
        private System.Windows.Forms.TextBox six_count;
        private System.Windows.Forms.TextBox seven_count;
        private System.Windows.Forms.TextBox eight_count;
    }
}
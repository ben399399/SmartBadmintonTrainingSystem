﻿namespace SmartBadmintonTrainingSystem
{
    partial class TestResult_number
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.combo_resultDatePick = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.f2_t3 = new System.Windows.Forms.TextBox();
            this.f2_t2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.f2_t1 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.s1_t3 = new System.Windows.Forms.TextBox();
            this.s1_t2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.s1_t1 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.f3_t3 = new System.Windows.Forms.TextBox();
            this.f3_t2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.f3_t1 = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.b3_t3 = new System.Windows.Forms.TextBox();
            this.b3_t2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.b3_t1 = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.b2_t3 = new System.Windows.Forms.TextBox();
            this.b2_t2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.b2_t1 = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.b1_t2 = new System.Windows.Forms.TextBox();
            this.b1_t3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.b1_t1 = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.s2_t3 = new System.Windows.Forms.TextBox();
            this.s2_t2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.s2_t1 = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tBox_uName = new System.Windows.Forms.TextBox();
            this.TotalTimeTextBox = new System.Windows.Forms.TextBox();
            this.f1_t1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.f1_t2 = new System.Windows.Forms.TextBox();
            this.f1_t3 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.plotView1 = new OxyPlot.WindowsForms.PlotView();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.combo_TimePick = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.b3_t6 = new System.Windows.Forms.TextBox();
            this.b3_t5 = new System.Windows.Forms.TextBox();
            this.b1_t6 = new System.Windows.Forms.TextBox();
            this.b3_t4 = new System.Windows.Forms.TextBox();
            this.b1_t5 = new System.Windows.Forms.TextBox();
            this.f3_t6 = new System.Windows.Forms.TextBox();
            this.b1_t4 = new System.Windows.Forms.TextBox();
            this.f1_t6 = new System.Windows.Forms.TextBox();
            this.f3_t5 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.f3_t4 = new System.Windows.Forms.TextBox();
            this.f1_t5 = new System.Windows.Forms.TextBox();
            this.f1_t4 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.b3_t12 = new System.Windows.Forms.TextBox();
            this.b3_t11 = new System.Windows.Forms.TextBox();
            this.b1_t12 = new System.Windows.Forms.TextBox();
            this.b3_t10 = new System.Windows.Forms.TextBox();
            this.b1_t11 = new System.Windows.Forms.TextBox();
            this.f3_t12 = new System.Windows.Forms.TextBox();
            this.b1_t10 = new System.Windows.Forms.TextBox();
            this.f1_t12 = new System.Windows.Forms.TextBox();
            this.f3_t11 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.f3_t10 = new System.Windows.Forms.TextBox();
            this.f1_t11 = new System.Windows.Forms.TextBox();
            this.f1_t10 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.TotalTimeTextBox2 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.f1_t9 = new System.Windows.Forms.TextBox();
            this.f1_t8 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.f1_t7 = new System.Windows.Forms.TextBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.f2_t6 = new System.Windows.Forms.TextBox();
            this.f2_t5 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.f2_t4 = new System.Windows.Forms.TextBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.f3_t9 = new System.Windows.Forms.TextBox();
            this.f3_t8 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.f3_t7 = new System.Windows.Forms.TextBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.s1_t6 = new System.Windows.Forms.TextBox();
            this.s1_t5 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.s1_t4 = new System.Windows.Forms.TextBox();
            this.panel20 = new System.Windows.Forms.Panel();
            this.s2_t6 = new System.Windows.Forms.TextBox();
            this.s2_t5 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.s2_t4 = new System.Windows.Forms.TextBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.b1_t8 = new System.Windows.Forms.TextBox();
            this.b1_t9 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.b1_t7 = new System.Windows.Forms.TextBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.b2_t6 = new System.Windows.Forms.TextBox();
            this.b2_t5 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.b2_t4 = new System.Windows.Forms.TextBox();
            this.panel23 = new System.Windows.Forms.Panel();
            this.b3_t9 = new System.Windows.Forms.TextBox();
            this.b3_t8 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.b3_t7 = new System.Windows.Forms.TextBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(226, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "날짜";
            // 
            // combo_resultDatePick
            // 
            this.combo_resultDatePick.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo_resultDatePick.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_resultDatePick.FormattingEnabled = true;
            this.combo_resultDatePick.Location = new System.Drawing.Point(266, 10);
            this.combo_resultDatePick.Name = "combo_resultDatePick";
            this.combo_resultDatePick.Size = new System.Drawing.Size(133, 20);
            this.combo_resultDatePick.TabIndex = 43;
            this.combo_resultDatePick.SelectedIndexChanged += new System.EventHandler(this.combo_resultDatePick_SelectedIndexChanged_1);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.f2_t3);
            this.panel2.Controls.Add(this.f2_t2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.f2_t1);
            this.panel2.Location = new System.Drawing.Point(173, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(84, 94);
            this.panel2.TabIndex = 45;
            // 
            // f2_t3
            // 
            this.f2_t3.Enabled = false;
            this.f2_t3.Location = new System.Drawing.Point(2, 71);
            this.f2_t3.Multiline = true;
            this.f2_t3.Name = "f2_t3";
            this.f2_t3.Size = new System.Drawing.Size(79, 20);
            this.f2_t3.TabIndex = 6;
            this.f2_t3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f2_t2
            // 
            this.f2_t2.Enabled = false;
            this.f2_t2.Location = new System.Drawing.Point(2, 49);
            this.f2_t2.Multiline = true;
            this.f2_t2.Name = "f2_t2";
            this.f2_t2.Size = new System.Drawing.Size(79, 20);
            this.f2_t2.TabIndex = 5;
            this.f2_t2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "F2";
            // 
            // f2_t1
            // 
            this.f2_t1.Enabled = false;
            this.f2_t1.Location = new System.Drawing.Point(2, 26);
            this.f2_t1.Multiline = true;
            this.f2_t1.Name = "f2_t1";
            this.f2_t1.Size = new System.Drawing.Size(79, 20);
            this.f2_t1.TabIndex = 4;
            this.f2_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.s1_t3);
            this.panel3.Controls.Add(this.s1_t2);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.s1_t1);
            this.panel3.Location = new System.Drawing.Point(343, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(84, 94);
            this.panel3.TabIndex = 47;
            // 
            // s1_t3
            // 
            this.s1_t3.Enabled = false;
            this.s1_t3.Location = new System.Drawing.Point(2, 71);
            this.s1_t3.Multiline = true;
            this.s1_t3.Name = "s1_t3";
            this.s1_t3.Size = new System.Drawing.Size(79, 20);
            this.s1_t3.TabIndex = 13;
            this.s1_t3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // s1_t2
            // 
            this.s1_t2.Enabled = false;
            this.s1_t2.Location = new System.Drawing.Point(2, 49);
            this.s1_t2.Multiline = true;
            this.s1_t2.Name = "s1_t2";
            this.s1_t2.Size = new System.Drawing.Size(79, 20);
            this.s1_t2.TabIndex = 9;
            this.s1_t2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "M1";
            // 
            // s1_t1
            // 
            this.s1_t1.Enabled = false;
            this.s1_t1.Location = new System.Drawing.Point(2, 26);
            this.s1_t1.Multiline = true;
            this.s1_t1.Name = "s1_t1";
            this.s1_t1.Size = new System.Drawing.Size(79, 20);
            this.s1_t1.TabIndex = 8;
            this.s1_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.f3_t3);
            this.panel4.Controls.Add(this.f3_t2);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.f3_t1);
            this.panel4.Location = new System.Drawing.Point(258, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(84, 94);
            this.panel4.TabIndex = 46;
            // 
            // f3_t3
            // 
            this.f3_t3.Enabled = false;
            this.f3_t3.Location = new System.Drawing.Point(2, 71);
            this.f3_t3.Multiline = true;
            this.f3_t3.Name = "f3_t3";
            this.f3_t3.Size = new System.Drawing.Size(79, 20);
            this.f3_t3.TabIndex = 12;
            this.f3_t3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.f3_t3.TextChanged += new System.EventHandler(this.textBox10_TextChanged);
            // 
            // f3_t2
            // 
            this.f3_t2.Enabled = false;
            this.f3_t2.Location = new System.Drawing.Point(3, 49);
            this.f3_t2.Multiline = true;
            this.f3_t2.Name = "f3_t2";
            this.f3_t2.Size = new System.Drawing.Size(79, 20);
            this.f3_t2.TabIndex = 7;
            this.f3_t2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "F3";
            // 
            // f3_t1
            // 
            this.f3_t1.Enabled = false;
            this.f3_t1.Location = new System.Drawing.Point(3, 26);
            this.f3_t1.Multiline = true;
            this.f3_t1.Name = "f3_t1";
            this.f3_t1.Size = new System.Drawing.Size(79, 20);
            this.f3_t1.TabIndex = 6;
            this.f3_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.b3_t3);
            this.panel5.Controls.Add(this.b3_t2);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.b3_t1);
            this.panel5.Location = new System.Drawing.Point(683, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(84, 94);
            this.panel5.TabIndex = 51;
            // 
            // b3_t3
            // 
            this.b3_t3.Enabled = false;
            this.b3_t3.Location = new System.Drawing.Point(2, 71);
            this.b3_t3.Multiline = true;
            this.b3_t3.Name = "b3_t3";
            this.b3_t3.Size = new System.Drawing.Size(79, 20);
            this.b3_t3.TabIndex = 58;
            this.b3_t3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b3_t2
            // 
            this.b3_t2.Enabled = false;
            this.b3_t2.Location = new System.Drawing.Point(2, 49);
            this.b3_t2.Multiline = true;
            this.b3_t2.Name = "b3_t2";
            this.b3_t2.Size = new System.Drawing.Size(79, 20);
            this.b3_t2.TabIndex = 17;
            this.b3_t2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 15);
            this.label9.TabIndex = 7;
            this.label9.Text = "B3";
            // 
            // b3_t1
            // 
            this.b3_t1.Enabled = false;
            this.b3_t1.Location = new System.Drawing.Point(2, 26);
            this.b3_t1.Multiline = true;
            this.b3_t1.Name = "b3_t1";
            this.b3_t1.Size = new System.Drawing.Size(79, 20);
            this.b3_t1.TabIndex = 16;
            this.b3_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.b2_t3);
            this.panel6.Controls.Add(this.b2_t2);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.b2_t1);
            this.panel6.Location = new System.Drawing.Point(598, 6);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(84, 94);
            this.panel6.TabIndex = 50;
            // 
            // b2_t3
            // 
            this.b2_t3.Enabled = false;
            this.b2_t3.Location = new System.Drawing.Point(2, 71);
            this.b2_t3.Multiline = true;
            this.b2_t3.Name = "b2_t3";
            this.b2_t3.Size = new System.Drawing.Size(79, 20);
            this.b2_t3.TabIndex = 57;
            this.b2_t3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b2_t2
            // 
            this.b2_t2.Enabled = false;
            this.b2_t2.Location = new System.Drawing.Point(2, 49);
            this.b2_t2.Multiline = true;
            this.b2_t2.Name = "b2_t2";
            this.b2_t2.Size = new System.Drawing.Size(79, 20);
            this.b2_t2.TabIndex = 15;
            this.b2_t2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 15);
            this.label8.TabIndex = 6;
            this.label8.Text = "B2";
            // 
            // b2_t1
            // 
            this.b2_t1.Enabled = false;
            this.b2_t1.Location = new System.Drawing.Point(2, 26);
            this.b2_t1.Multiline = true;
            this.b2_t1.Name = "b2_t1";
            this.b2_t1.Size = new System.Drawing.Size(79, 20);
            this.b2_t1.TabIndex = 14;
            this.b2_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.b1_t2);
            this.panel7.Controls.Add(this.b1_t3);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.b1_t1);
            this.panel7.Location = new System.Drawing.Point(513, 6);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(84, 94);
            this.panel7.TabIndex = 49;
            // 
            // b1_t2
            // 
            this.b1_t2.Enabled = false;
            this.b1_t2.Location = new System.Drawing.Point(2, 49);
            this.b1_t2.Multiline = true;
            this.b1_t2.Name = "b1_t2";
            this.b1_t2.Size = new System.Drawing.Size(79, 20);
            this.b1_t2.TabIndex = 13;
            this.b1_t2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b1_t3
            // 
            this.b1_t3.Enabled = false;
            this.b1_t3.Location = new System.Drawing.Point(2, 71);
            this.b1_t3.Multiline = true;
            this.b1_t3.Name = "b1_t3";
            this.b1_t3.Size = new System.Drawing.Size(79, 20);
            this.b1_t3.TabIndex = 56;
            this.b1_t3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "B1";
            // 
            // b1_t1
            // 
            this.b1_t1.Enabled = false;
            this.b1_t1.Location = new System.Drawing.Point(2, 26);
            this.b1_t1.Multiline = true;
            this.b1_t1.Name = "b1_t1";
            this.b1_t1.Size = new System.Drawing.Size(79, 20);
            this.b1_t1.TabIndex = 12;
            this.b1_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.s2_t3);
            this.panel8.Controls.Add(this.s2_t2);
            this.panel8.Controls.Add(this.label6);
            this.panel8.Controls.Add(this.s2_t1);
            this.panel8.Location = new System.Drawing.Point(428, 6);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(84, 94);
            this.panel8.TabIndex = 48;
            // 
            // s2_t3
            // 
            this.s2_t3.Enabled = false;
            this.s2_t3.Location = new System.Drawing.Point(2, 71);
            this.s2_t3.Multiline = true;
            this.s2_t3.Name = "s2_t3";
            this.s2_t3.Size = new System.Drawing.Size(79, 20);
            this.s2_t3.TabIndex = 14;
            this.s2_t3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // s2_t2
            // 
            this.s2_t2.Enabled = false;
            this.s2_t2.Location = new System.Drawing.Point(2, 49);
            this.s2_t2.Multiline = true;
            this.s2_t2.Name = "s2_t2";
            this.s2_t2.Size = new System.Drawing.Size(79, 20);
            this.s2_t2.TabIndex = 11;
            this.s2_t2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "M2";
            // 
            // s2_t1
            // 
            this.s2_t1.Enabled = false;
            this.s2_t1.Location = new System.Drawing.Point(2, 26);
            this.s2_t1.Multiline = true;
            this.s2_t1.Name = "s2_t1";
            this.s2_t1.Size = new System.Drawing.Size(79, 20);
            this.s2_t1.TabIndex = 10;
            this.s2_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.textBox4);
            this.panel9.Controls.Add(this.textBox2);
            this.panel9.Controls.Add(this.textBox1);
            this.panel9.Location = new System.Drawing.Point(3, 6);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(84, 94);
            this.panel9.TabIndex = 52;
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(3, 71);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(79, 20);
            this.textBox4.TabIndex = 2;
            this.textBox4.Text = "구간합계";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(3, 49);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(79, 20);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "후진구간";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(3, 26);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(79, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "전진구간";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(8, 174);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(79, 20);
            this.textBox3.TabIndex = 56;
            this.textBox3.Text = "전체결과";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(17, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 18);
            this.label10.TabIndex = 53;
            this.label10.Text = "아이디";
            // 
            // tBox_uName
            // 
            this.tBox_uName.BackColor = System.Drawing.Color.White;
            this.tBox_uName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tBox_uName.Enabled = false;
            this.tBox_uName.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBox_uName.Location = new System.Drawing.Point(66, 11);
            this.tBox_uName.Name = "tBox_uName";
            this.tBox_uName.Size = new System.Drawing.Size(133, 14);
            this.tBox_uName.TabIndex = 54;
            this.tBox_uName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TotalTimeTextBox
            // 
            this.TotalTimeTextBox.Enabled = false;
            this.TotalTimeTextBox.Location = new System.Drawing.Point(92, 174);
            this.TotalTimeTextBox.Multiline = true;
            this.TotalTimeTextBox.Name = "TotalTimeTextBox";
            this.TotalTimeTextBox.Size = new System.Drawing.Size(674, 20);
            this.TotalTimeTextBox.TabIndex = 57;
            this.TotalTimeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f1_t1
            // 
            this.f1_t1.Enabled = false;
            this.f1_t1.Location = new System.Drawing.Point(2, 26);
            this.f1_t1.Multiline = true;
            this.f1_t1.Name = "f1_t1";
            this.f1_t1.Size = new System.Drawing.Size(79, 20);
            this.f1_t1.TabIndex = 2;
            this.f1_t1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "F1";
            // 
            // f1_t2
            // 
            this.f1_t2.Enabled = false;
            this.f1_t2.Location = new System.Drawing.Point(2, 49);
            this.f1_t2.Multiline = true;
            this.f1_t2.Name = "f1_t2";
            this.f1_t2.Size = new System.Drawing.Size(79, 20);
            this.f1_t2.TabIndex = 3;
            this.f1_t2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f1_t3
            // 
            this.f1_t3.Enabled = false;
            this.f1_t3.Location = new System.Drawing.Point(2, 71);
            this.f1_t3.Multiline = true;
            this.f1_t3.Name = "f1_t3";
            this.f1_t3.Size = new System.Drawing.Size(79, 20);
            this.f1_t3.TabIndex = 4;
            this.f1_t3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.f1_t3);
            this.panel1.Controls.Add(this.f1_t2);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.f1_t1);
            this.panel1.Location = new System.Drawing.Point(88, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(84, 94);
            this.panel1.TabIndex = 44;
            // 
            // plotView1
            // 
            this.plotView1.Location = new System.Drawing.Point(36, 296);
            this.plotView1.Name = "plotView1";
            this.plotView1.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.plotView1.Size = new System.Drawing.Size(1549, 434);
            this.plotView1.TabIndex = 58;
            this.plotView1.Text = "plotView1";
            this.plotView1.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.plotView1.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.plotView1.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(613, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(61, 26);
            this.button1.TabIndex = 59;
            this.button1.Text = "검색";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(8, 105);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 61;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            this.checkBox1.Click += new System.EventHandler(this.checkBox1_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(8, 128);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 62;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            this.checkBox2.Click += new System.EventHandler(this.checkBox2_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(8, 150);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 63;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            this.checkBox3.Click += new System.EventHandler(this.checkBox3_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.panel10.Location = new System.Drawing.Point(266, 28);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(133, 1);
            this.panel10.TabIndex = 102;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.panel11.Location = new System.Drawing.Point(66, 26);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(133, 1);
            this.panel11.TabIndex = 103;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.panel12.Location = new System.Drawing.Point(453, 28);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(133, 1);
            this.panel12.TabIndex = 106;
            // 
            // combo_TimePick
            // 
            this.combo_TimePick.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo_TimePick.Font = new System.Drawing.Font("Gulim", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_TimePick.FormattingEnabled = true;
            this.combo_TimePick.Location = new System.Drawing.Point(453, 10);
            this.combo_TimePick.Name = "combo_TimePick";
            this.combo_TimePick.Size = new System.Drawing.Size(133, 20);
            this.combo_TimePick.TabIndex = 105;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(413, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 18);
            this.label11.TabIndex = 104;
            this.label11.Text = "회차";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.b3_t6);
            this.panel13.Controls.Add(this.b3_t5);
            this.panel13.Controls.Add(this.b1_t6);
            this.panel13.Controls.Add(this.b3_t4);
            this.panel13.Controls.Add(this.b1_t5);
            this.panel13.Controls.Add(this.f3_t6);
            this.panel13.Controls.Add(this.b1_t4);
            this.panel13.Controls.Add(this.f1_t6);
            this.panel13.Controls.Add(this.f3_t5);
            this.panel13.Controls.Add(this.textBox34);
            this.panel13.Controls.Add(this.f3_t4);
            this.panel13.Controls.Add(this.f1_t5);
            this.panel13.Controls.Add(this.f1_t4);
            this.panel13.Controls.Add(this.panel9);
            this.panel13.Controls.Add(this.textBox36);
            this.panel13.Controls.Add(this.panel1);
            this.panel13.Controls.Add(this.panel2);
            this.panel13.Controls.Add(this.textBox38);
            this.panel13.Controls.Add(this.panel4);
            this.panel13.Controls.Add(this.panel3);
            this.panel13.Controls.Add(this.panel8);
            this.panel13.Controls.Add(this.panel7);
            this.panel13.Controls.Add(this.panel6);
            this.panel13.Controls.Add(this.panel5);
            this.panel13.Controls.Add(this.TotalTimeTextBox);
            this.panel13.Controls.Add(this.textBox3);
            this.panel13.Location = new System.Drawing.Point(36, 68);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(770, 208);
            this.panel13.TabIndex = 107;
            // 
            // b3_t6
            // 
            this.b3_t6.Enabled = false;
            this.b3_t6.Location = new System.Drawing.Point(685, 151);
            this.b3_t6.Multiline = true;
            this.b3_t6.Name = "b3_t6";
            this.b3_t6.Size = new System.Drawing.Size(79, 20);
            this.b3_t6.TabIndex = 61;
            this.b3_t6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b3_t5
            // 
            this.b3_t5.Enabled = false;
            this.b3_t5.Location = new System.Drawing.Point(685, 129);
            this.b3_t5.Multiline = true;
            this.b3_t5.Name = "b3_t5";
            this.b3_t5.Size = new System.Drawing.Size(79, 20);
            this.b3_t5.TabIndex = 60;
            this.b3_t5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b1_t6
            // 
            this.b1_t6.Enabled = false;
            this.b1_t6.Location = new System.Drawing.Point(516, 151);
            this.b1_t6.Multiline = true;
            this.b1_t6.Name = "b1_t6";
            this.b1_t6.Size = new System.Drawing.Size(79, 20);
            this.b1_t6.TabIndex = 17;
            this.b1_t6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b3_t4
            // 
            this.b3_t4.Enabled = false;
            this.b3_t4.Location = new System.Drawing.Point(685, 106);
            this.b3_t4.Multiline = true;
            this.b3_t4.Name = "b3_t4";
            this.b3_t4.Size = new System.Drawing.Size(79, 20);
            this.b3_t4.TabIndex = 59;
            this.b3_t4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b1_t5
            // 
            this.b1_t5.Enabled = false;
            this.b1_t5.Location = new System.Drawing.Point(516, 129);
            this.b1_t5.Multiline = true;
            this.b1_t5.Name = "b1_t5";
            this.b1_t5.Size = new System.Drawing.Size(79, 20);
            this.b1_t5.TabIndex = 16;
            this.b1_t5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f3_t6
            // 
            this.f3_t6.Enabled = false;
            this.f3_t6.Location = new System.Drawing.Point(261, 151);
            this.f3_t6.Multiline = true;
            this.f3_t6.Name = "f3_t6";
            this.f3_t6.Size = new System.Drawing.Size(79, 20);
            this.f3_t6.TabIndex = 15;
            this.f3_t6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b1_t4
            // 
            this.b1_t4.Enabled = false;
            this.b1_t4.Location = new System.Drawing.Point(516, 106);
            this.b1_t4.Multiline = true;
            this.b1_t4.Name = "b1_t4";
            this.b1_t4.Size = new System.Drawing.Size(79, 20);
            this.b1_t4.TabIndex = 15;
            this.b1_t4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f1_t6
            // 
            this.f1_t6.Enabled = false;
            this.f1_t6.Location = new System.Drawing.Point(92, 151);
            this.f1_t6.Multiline = true;
            this.f1_t6.Name = "f1_t6";
            this.f1_t6.Size = new System.Drawing.Size(79, 20);
            this.f1_t6.TabIndex = 7;
            this.f1_t6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f3_t5
            // 
            this.f3_t5.Enabled = false;
            this.f3_t5.Location = new System.Drawing.Point(261, 129);
            this.f3_t5.Multiline = true;
            this.f3_t5.Name = "f3_t5";
            this.f3_t5.Size = new System.Drawing.Size(79, 20);
            this.f3_t5.TabIndex = 14;
            this.f3_t5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox34
            // 
            this.textBox34.Enabled = false;
            this.textBox34.Location = new System.Drawing.Point(8, 106);
            this.textBox34.Multiline = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(79, 20);
            this.textBox34.TabIndex = 5;
            this.textBox34.Text = "전진구간";
            this.textBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f3_t4
            // 
            this.f3_t4.Enabled = false;
            this.f3_t4.Location = new System.Drawing.Point(261, 106);
            this.f3_t4.Multiline = true;
            this.f3_t4.Name = "f3_t4";
            this.f3_t4.Size = new System.Drawing.Size(79, 20);
            this.f3_t4.TabIndex = 13;
            this.f3_t4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f1_t5
            // 
            this.f1_t5.Enabled = false;
            this.f1_t5.Location = new System.Drawing.Point(92, 129);
            this.f1_t5.Multiline = true;
            this.f1_t5.Name = "f1_t5";
            this.f1_t5.Size = new System.Drawing.Size(79, 20);
            this.f1_t5.TabIndex = 6;
            this.f1_t5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f1_t4
            // 
            this.f1_t4.Enabled = false;
            this.f1_t4.Location = new System.Drawing.Point(92, 106);
            this.f1_t4.Multiline = true;
            this.f1_t4.Name = "f1_t4";
            this.f1_t4.Size = new System.Drawing.Size(79, 20);
            this.f1_t4.TabIndex = 5;
            this.f1_t4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox36
            // 
            this.textBox36.Enabled = false;
            this.textBox36.Location = new System.Drawing.Point(8, 129);
            this.textBox36.Multiline = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(79, 20);
            this.textBox36.TabIndex = 6;
            this.textBox36.Text = "후진구간";
            this.textBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox38
            // 
            this.textBox38.Enabled = false;
            this.textBox38.Location = new System.Drawing.Point(8, 151);
            this.textBox38.Multiline = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(79, 20);
            this.textBox38.TabIndex = 8;
            this.textBox38.Text = "구간합계";
            this.textBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.b3_t12);
            this.panel14.Controls.Add(this.b3_t11);
            this.panel14.Controls.Add(this.b1_t12);
            this.panel14.Controls.Add(this.b3_t10);
            this.panel14.Controls.Add(this.b1_t11);
            this.panel14.Controls.Add(this.f3_t12);
            this.panel14.Controls.Add(this.b1_t10);
            this.panel14.Controls.Add(this.f1_t12);
            this.panel14.Controls.Add(this.f3_t11);
            this.panel14.Controls.Add(this.textBox56);
            this.panel14.Controls.Add(this.f3_t10);
            this.panel14.Controls.Add(this.f1_t11);
            this.panel14.Controls.Add(this.f1_t10);
            this.panel14.Controls.Add(this.textBox60);
            this.panel14.Controls.Add(this.textBox61);
            this.panel14.Controls.Add(this.TotalTimeTextBox2);
            this.panel14.Controls.Add(this.textBox63);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Controls.Add(this.panel16);
            this.panel14.Controls.Add(this.panel17);
            this.panel14.Controls.Add(this.panel18);
            this.panel14.Controls.Add(this.panel19);
            this.panel14.Controls.Add(this.panel20);
            this.panel14.Controls.Add(this.panel21);
            this.panel14.Controls.Add(this.panel22);
            this.panel14.Controls.Add(this.panel23);
            this.panel14.Location = new System.Drawing.Point(812, 68);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(770, 208);
            this.panel14.TabIndex = 108;
            // 
            // b3_t12
            // 
            this.b3_t12.Enabled = false;
            this.b3_t12.Location = new System.Drawing.Point(685, 151);
            this.b3_t12.Multiline = true;
            this.b3_t12.Name = "b3_t12";
            this.b3_t12.Size = new System.Drawing.Size(79, 20);
            this.b3_t12.TabIndex = 78;
            this.b3_t12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b3_t11
            // 
            this.b3_t11.Enabled = false;
            this.b3_t11.Location = new System.Drawing.Point(685, 129);
            this.b3_t11.Multiline = true;
            this.b3_t11.Name = "b3_t11";
            this.b3_t11.Size = new System.Drawing.Size(79, 20);
            this.b3_t11.TabIndex = 77;
            this.b3_t11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b1_t12
            // 
            this.b1_t12.Enabled = false;
            this.b1_t12.Location = new System.Drawing.Point(514, 151);
            this.b1_t12.Multiline = true;
            this.b1_t12.Name = "b1_t12";
            this.b1_t12.Size = new System.Drawing.Size(79, 20);
            this.b1_t12.TabIndex = 73;
            this.b1_t12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b3_t10
            // 
            this.b3_t10.Enabled = false;
            this.b3_t10.Location = new System.Drawing.Point(685, 106);
            this.b3_t10.Multiline = true;
            this.b3_t10.Name = "b3_t10";
            this.b3_t10.Size = new System.Drawing.Size(79, 20);
            this.b3_t10.TabIndex = 76;
            this.b3_t10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b1_t11
            // 
            this.b1_t11.Enabled = false;
            this.b1_t11.Location = new System.Drawing.Point(514, 129);
            this.b1_t11.Multiline = true;
            this.b1_t11.Name = "b1_t11";
            this.b1_t11.Size = new System.Drawing.Size(79, 20);
            this.b1_t11.TabIndex = 72;
            this.b1_t11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f3_t12
            // 
            this.f3_t12.Enabled = false;
            this.f3_t12.Location = new System.Drawing.Point(259, 151);
            this.f3_t12.Multiline = true;
            this.f3_t12.Name = "f3_t12";
            this.f3_t12.Size = new System.Drawing.Size(79, 20);
            this.f3_t12.TabIndex = 70;
            this.f3_t12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b1_t10
            // 
            this.b1_t10.Enabled = false;
            this.b1_t10.Location = new System.Drawing.Point(514, 106);
            this.b1_t10.Multiline = true;
            this.b1_t10.Name = "b1_t10";
            this.b1_t10.Size = new System.Drawing.Size(79, 20);
            this.b1_t10.TabIndex = 71;
            this.b1_t10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f1_t12
            // 
            this.f1_t12.Enabled = false;
            this.f1_t12.Location = new System.Drawing.Point(90, 151);
            this.f1_t12.Multiline = true;
            this.f1_t12.Name = "f1_t12";
            this.f1_t12.Size = new System.Drawing.Size(79, 20);
            this.f1_t12.TabIndex = 66;
            this.f1_t12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f3_t11
            // 
            this.f3_t11.Enabled = false;
            this.f3_t11.Location = new System.Drawing.Point(259, 129);
            this.f3_t11.Multiline = true;
            this.f3_t11.Name = "f3_t11";
            this.f3_t11.Size = new System.Drawing.Size(79, 20);
            this.f3_t11.TabIndex = 69;
            this.f3_t11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox56
            // 
            this.textBox56.Enabled = false;
            this.textBox56.Location = new System.Drawing.Point(6, 106);
            this.textBox56.Multiline = true;
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(79, 20);
            this.textBox56.TabIndex = 63;
            this.textBox56.Text = "전진구간";
            this.textBox56.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f3_t10
            // 
            this.f3_t10.Enabled = false;
            this.f3_t10.Location = new System.Drawing.Point(259, 106);
            this.f3_t10.Multiline = true;
            this.f3_t10.Name = "f3_t10";
            this.f3_t10.Size = new System.Drawing.Size(79, 20);
            this.f3_t10.TabIndex = 68;
            this.f3_t10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f1_t11
            // 
            this.f1_t11.Enabled = false;
            this.f1_t11.Location = new System.Drawing.Point(90, 129);
            this.f1_t11.Multiline = true;
            this.f1_t11.Name = "f1_t11";
            this.f1_t11.Size = new System.Drawing.Size(79, 20);
            this.f1_t11.TabIndex = 65;
            this.f1_t11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f1_t10
            // 
            this.f1_t10.Enabled = false;
            this.f1_t10.Location = new System.Drawing.Point(90, 106);
            this.f1_t10.Multiline = true;
            this.f1_t10.Name = "f1_t10";
            this.f1_t10.Size = new System.Drawing.Size(79, 20);
            this.f1_t10.TabIndex = 62;
            this.f1_t10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox60
            // 
            this.textBox60.Enabled = false;
            this.textBox60.Location = new System.Drawing.Point(6, 129);
            this.textBox60.Multiline = true;
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(79, 20);
            this.textBox60.TabIndex = 64;
            this.textBox60.Text = "후진구간";
            this.textBox60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox61
            // 
            this.textBox61.Enabled = false;
            this.textBox61.Location = new System.Drawing.Point(6, 151);
            this.textBox61.Multiline = true;
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(79, 20);
            this.textBox61.TabIndex = 67;
            this.textBox61.Text = "구간합계";
            this.textBox61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TotalTimeTextBox2
            // 
            this.TotalTimeTextBox2.Enabled = false;
            this.TotalTimeTextBox2.Location = new System.Drawing.Point(90, 174);
            this.TotalTimeTextBox2.Multiline = true;
            this.TotalTimeTextBox2.Name = "TotalTimeTextBox2";
            this.TotalTimeTextBox2.Size = new System.Drawing.Size(674, 20);
            this.TotalTimeTextBox2.TabIndex = 75;
            this.TotalTimeTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox63
            // 
            this.textBox63.Enabled = false;
            this.textBox63.Location = new System.Drawing.Point(6, 174);
            this.textBox63.Multiline = true;
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(79, 20);
            this.textBox63.TabIndex = 74;
            this.textBox63.Text = "전체결과";
            this.textBox63.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.textBox5);
            this.panel15.Controls.Add(this.textBox6);
            this.panel15.Controls.Add(this.textBox7);
            this.panel15.Location = new System.Drawing.Point(3, 6);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(84, 94);
            this.panel15.TabIndex = 52;
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(3, 71);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(79, 20);
            this.textBox5.TabIndex = 2;
            this.textBox5.Text = "구간합계";
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(3, 49);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(79, 20);
            this.textBox6.TabIndex = 1;
            this.textBox6.Text = "후진구간";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(3, 26);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(79, 20);
            this.textBox7.TabIndex = 0;
            this.textBox7.Text = "전진구간";
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.f1_t9);
            this.panel16.Controls.Add(this.f1_t8);
            this.panel16.Controls.Add(this.label12);
            this.panel16.Controls.Add(this.f1_t7);
            this.panel16.Location = new System.Drawing.Point(88, 6);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(84, 94);
            this.panel16.TabIndex = 44;
            // 
            // f1_t9
            // 
            this.f1_t9.Enabled = false;
            this.f1_t9.Location = new System.Drawing.Point(2, 71);
            this.f1_t9.Multiline = true;
            this.f1_t9.Name = "f1_t9";
            this.f1_t9.Size = new System.Drawing.Size(79, 20);
            this.f1_t9.TabIndex = 4;
            this.f1_t9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f1_t8
            // 
            this.f1_t8.Enabled = false;
            this.f1_t8.Location = new System.Drawing.Point(2, 49);
            this.f1_t8.Multiline = true;
            this.f1_t8.Name = "f1_t8";
            this.f1_t8.Size = new System.Drawing.Size(79, 20);
            this.f1_t8.TabIndex = 3;
            this.f1_t8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(29, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "F1";
            // 
            // f1_t7
            // 
            this.f1_t7.Enabled = false;
            this.f1_t7.Location = new System.Drawing.Point(2, 26);
            this.f1_t7.Multiline = true;
            this.f1_t7.Name = "f1_t7";
            this.f1_t7.Size = new System.Drawing.Size(79, 20);
            this.f1_t7.TabIndex = 2;
            this.f1_t7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.f2_t6);
            this.panel17.Controls.Add(this.f2_t5);
            this.panel17.Controls.Add(this.label13);
            this.panel17.Controls.Add(this.f2_t4);
            this.panel17.Location = new System.Drawing.Point(173, 6);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(84, 94);
            this.panel17.TabIndex = 45;
            // 
            // f2_t6
            // 
            this.f2_t6.Enabled = false;
            this.f2_t6.Location = new System.Drawing.Point(2, 71);
            this.f2_t6.Multiline = true;
            this.f2_t6.Name = "f2_t6";
            this.f2_t6.Size = new System.Drawing.Size(79, 20);
            this.f2_t6.TabIndex = 6;
            this.f2_t6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f2_t5
            // 
            this.f2_t5.Enabled = false;
            this.f2_t5.Location = new System.Drawing.Point(2, 49);
            this.f2_t5.Multiline = true;
            this.f2_t5.Name = "f2_t5";
            this.f2_t5.Size = new System.Drawing.Size(79, 20);
            this.f2_t5.TabIndex = 5;
            this.f2_t5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(29, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 15);
            this.label13.TabIndex = 1;
            this.label13.Text = "F2";
            // 
            // f2_t4
            // 
            this.f2_t4.Enabled = false;
            this.f2_t4.Location = new System.Drawing.Point(2, 26);
            this.f2_t4.Multiline = true;
            this.f2_t4.Name = "f2_t4";
            this.f2_t4.Size = new System.Drawing.Size(79, 20);
            this.f2_t4.TabIndex = 4;
            this.f2_t4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.f3_t9);
            this.panel18.Controls.Add(this.f3_t8);
            this.panel18.Controls.Add(this.label14);
            this.panel18.Controls.Add(this.f3_t7);
            this.panel18.Location = new System.Drawing.Point(258, 6);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(84, 94);
            this.panel18.TabIndex = 46;
            // 
            // f3_t9
            // 
            this.f3_t9.Enabled = false;
            this.f3_t9.Location = new System.Drawing.Point(2, 71);
            this.f3_t9.Multiline = true;
            this.f3_t9.Name = "f3_t9";
            this.f3_t9.Size = new System.Drawing.Size(79, 20);
            this.f3_t9.TabIndex = 12;
            this.f3_t9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // f3_t8
            // 
            this.f3_t8.Enabled = false;
            this.f3_t8.Location = new System.Drawing.Point(3, 49);
            this.f3_t8.Multiline = true;
            this.f3_t8.Name = "f3_t8";
            this.f3_t8.Size = new System.Drawing.Size(79, 20);
            this.f3_t8.TabIndex = 7;
            this.f3_t8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(29, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 15);
            this.label14.TabIndex = 2;
            this.label14.Text = "F3";
            // 
            // f3_t7
            // 
            this.f3_t7.Enabled = false;
            this.f3_t7.Location = new System.Drawing.Point(3, 26);
            this.f3_t7.Multiline = true;
            this.f3_t7.Name = "f3_t7";
            this.f3_t7.Size = new System.Drawing.Size(79, 20);
            this.f3_t7.TabIndex = 6;
            this.f3_t7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.s1_t6);
            this.panel19.Controls.Add(this.s1_t5);
            this.panel19.Controls.Add(this.label15);
            this.panel19.Controls.Add(this.s1_t4);
            this.panel19.Location = new System.Drawing.Point(343, 6);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(84, 94);
            this.panel19.TabIndex = 47;
            // 
            // s1_t6
            // 
            this.s1_t6.Enabled = false;
            this.s1_t6.Location = new System.Drawing.Point(2, 71);
            this.s1_t6.Multiline = true;
            this.s1_t6.Name = "s1_t6";
            this.s1_t6.Size = new System.Drawing.Size(79, 20);
            this.s1_t6.TabIndex = 13;
            this.s1_t6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // s1_t5
            // 
            this.s1_t5.Enabled = false;
            this.s1_t5.Location = new System.Drawing.Point(2, 49);
            this.s1_t5.Multiline = true;
            this.s1_t5.Name = "s1_t5";
            this.s1_t5.Size = new System.Drawing.Size(79, 20);
            this.s1_t5.TabIndex = 9;
            this.s1_t5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(29, 3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 15);
            this.label15.TabIndex = 3;
            this.label15.Text = "M1";
            // 
            // s1_t4
            // 
            this.s1_t4.Enabled = false;
            this.s1_t4.Location = new System.Drawing.Point(2, 26);
            this.s1_t4.Multiline = true;
            this.s1_t4.Name = "s1_t4";
            this.s1_t4.Size = new System.Drawing.Size(79, 20);
            this.s1_t4.TabIndex = 8;
            this.s1_t4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.s2_t6);
            this.panel20.Controls.Add(this.s2_t5);
            this.panel20.Controls.Add(this.label16);
            this.panel20.Controls.Add(this.s2_t4);
            this.panel20.Location = new System.Drawing.Point(428, 6);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(84, 94);
            this.panel20.TabIndex = 48;
            // 
            // s2_t6
            // 
            this.s2_t6.Enabled = false;
            this.s2_t6.Location = new System.Drawing.Point(2, 71);
            this.s2_t6.Multiline = true;
            this.s2_t6.Name = "s2_t6";
            this.s2_t6.Size = new System.Drawing.Size(79, 20);
            this.s2_t6.TabIndex = 14;
            this.s2_t6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // s2_t5
            // 
            this.s2_t5.Enabled = false;
            this.s2_t5.Location = new System.Drawing.Point(2, 49);
            this.s2_t5.Multiline = true;
            this.s2_t5.Name = "s2_t5";
            this.s2_t5.Size = new System.Drawing.Size(79, 20);
            this.s2_t5.TabIndex = 11;
            this.s2_t5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(29, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(25, 15);
            this.label16.TabIndex = 4;
            this.label16.Text = "M2";
            // 
            // s2_t4
            // 
            this.s2_t4.Enabled = false;
            this.s2_t4.Location = new System.Drawing.Point(2, 26);
            this.s2_t4.Multiline = true;
            this.s2_t4.Name = "s2_t4";
            this.s2_t4.Size = new System.Drawing.Size(79, 20);
            this.s2_t4.TabIndex = 10;
            this.s2_t4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.b1_t8);
            this.panel21.Controls.Add(this.b1_t9);
            this.panel21.Controls.Add(this.label17);
            this.panel21.Controls.Add(this.b1_t7);
            this.panel21.Location = new System.Drawing.Point(513, 6);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(84, 94);
            this.panel21.TabIndex = 49;
            // 
            // b1_t8
            // 
            this.b1_t8.Enabled = false;
            this.b1_t8.Location = new System.Drawing.Point(2, 49);
            this.b1_t8.Multiline = true;
            this.b1_t8.Name = "b1_t8";
            this.b1_t8.Size = new System.Drawing.Size(79, 20);
            this.b1_t8.TabIndex = 13;
            this.b1_t8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b1_t9
            // 
            this.b1_t9.Enabled = false;
            this.b1_t9.Location = new System.Drawing.Point(2, 71);
            this.b1_t9.Multiline = true;
            this.b1_t9.Name = "b1_t9";
            this.b1_t9.Size = new System.Drawing.Size(79, 20);
            this.b1_t9.TabIndex = 56;
            this.b1_t9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(29, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 15);
            this.label17.TabIndex = 5;
            this.label17.Text = "B1";
            // 
            // b1_t7
            // 
            this.b1_t7.Enabled = false;
            this.b1_t7.Location = new System.Drawing.Point(2, 26);
            this.b1_t7.Multiline = true;
            this.b1_t7.Name = "b1_t7";
            this.b1_t7.Size = new System.Drawing.Size(79, 20);
            this.b1_t7.TabIndex = 12;
            this.b1_t7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.b2_t6);
            this.panel22.Controls.Add(this.b2_t5);
            this.panel22.Controls.Add(this.label18);
            this.panel22.Controls.Add(this.b2_t4);
            this.panel22.Location = new System.Drawing.Point(598, 6);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(84, 94);
            this.panel22.TabIndex = 50;
            // 
            // b2_t6
            // 
            this.b2_t6.Enabled = false;
            this.b2_t6.Location = new System.Drawing.Point(2, 71);
            this.b2_t6.Multiline = true;
            this.b2_t6.Name = "b2_t6";
            this.b2_t6.Size = new System.Drawing.Size(79, 20);
            this.b2_t6.TabIndex = 57;
            this.b2_t6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b2_t5
            // 
            this.b2_t5.Enabled = false;
            this.b2_t5.Location = new System.Drawing.Point(2, 49);
            this.b2_t5.Multiline = true;
            this.b2_t5.Name = "b2_t5";
            this.b2_t5.Size = new System.Drawing.Size(79, 20);
            this.b2_t5.TabIndex = 15;
            this.b2_t5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(29, 3);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 15);
            this.label18.TabIndex = 6;
            this.label18.Text = "B2";
            // 
            // b2_t4
            // 
            this.b2_t4.Enabled = false;
            this.b2_t4.Location = new System.Drawing.Point(2, 26);
            this.b2_t4.Multiline = true;
            this.b2_t4.Name = "b2_t4";
            this.b2_t4.Size = new System.Drawing.Size(79, 20);
            this.b2_t4.TabIndex = 14;
            this.b2_t4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.b3_t9);
            this.panel23.Controls.Add(this.b3_t8);
            this.panel23.Controls.Add(this.label19);
            this.panel23.Controls.Add(this.b3_t7);
            this.panel23.Location = new System.Drawing.Point(683, 6);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(84, 94);
            this.panel23.TabIndex = 51;
            // 
            // b3_t9
            // 
            this.b3_t9.Enabled = false;
            this.b3_t9.Location = new System.Drawing.Point(2, 71);
            this.b3_t9.Multiline = true;
            this.b3_t9.Name = "b3_t9";
            this.b3_t9.Size = new System.Drawing.Size(79, 20);
            this.b3_t9.TabIndex = 58;
            this.b3_t9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // b3_t8
            // 
            this.b3_t8.Enabled = false;
            this.b3_t8.Location = new System.Drawing.Point(2, 49);
            this.b3_t8.Multiline = true;
            this.b3_t8.Name = "b3_t8";
            this.b3_t8.Size = new System.Drawing.Size(79, 20);
            this.b3_t8.TabIndex = 17;
            this.b3_t8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(29, 3);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(22, 15);
            this.label19.TabIndex = 7;
            this.label19.Text = "B3";
            // 
            // b3_t7
            // 
            this.b3_t7.Enabled = false;
            this.b3_t7.Location = new System.Drawing.Point(2, 26);
            this.b3_t7.Multiline = true;
            this.b3_t7.Name = "b3_t7";
            this.b3_t7.Size = new System.Drawing.Size(79, 20);
            this.b3_t7.TabIndex = 16;
            this.b3_t7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.Tomato;
            this.panel24.Controls.Add(this.label20);
            this.panel24.Location = new System.Drawing.Point(36, 41);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(770, 21);
            this.panel24.TabIndex = 109;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(360, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 15);
            this.label20.TabIndex = 0;
            this.label20.Text = "1회차";
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            this.panel25.Controls.Add(this.label21);
            this.panel25.Location = new System.Drawing.Point(812, 41);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(770, 21);
            this.panel25.TabIndex = 110;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(360, 3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(38, 15);
            this.label21.TabIndex = 1;
            this.label21.Text = "2회차";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(87)))), ((int)(((byte)(34)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(681, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 26);
            this.button2.TabIndex = 111;
            this.button2.Text = ".csv로 출력";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // TestResult_number
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1597, 758);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.combo_TimePick);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.plotView1);
            this.Controls.Add(this.tBox_uName);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.combo_resultDatePick);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Name = "TestResult_number";
            this.Text = "테스트 결과 - 구간별";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TestResult_number_FormClosed);
            this.Load += new System.EventHandler(this.TestResult_number_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox combo_resultDatePick;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox f2_t2;
        private System.Windows.Forms.TextBox f2_t1;
        private System.Windows.Forms.TextBox f3_t2;
        private System.Windows.Forms.TextBox f3_t1;
        private System.Windows.Forms.TextBox s1_t2;
        private System.Windows.Forms.TextBox s1_t1;
        private System.Windows.Forms.TextBox b3_t2;
        private System.Windows.Forms.TextBox b3_t1;
        private System.Windows.Forms.TextBox b2_t2;
        private System.Windows.Forms.TextBox b2_t1;
        private System.Windows.Forms.TextBox b1_t2;
        private System.Windows.Forms.TextBox b1_t1;
        private System.Windows.Forms.TextBox s2_t2;
        private System.Windows.Forms.TextBox s2_t1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tBox_uName;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox f2_t3;
        private System.Windows.Forms.TextBox s1_t3;
        private System.Windows.Forms.TextBox f3_t3;
        private System.Windows.Forms.TextBox s2_t3;
        private System.Windows.Forms.TextBox TotalTimeTextBox;
        private System.Windows.Forms.TextBox b3_t3;
        private System.Windows.Forms.TextBox b2_t3;
        private System.Windows.Forms.TextBox b1_t3;
        private System.Windows.Forms.TextBox f1_t1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox f1_t2;
        private System.Windows.Forms.TextBox f1_t3;
        private System.Windows.Forms.Panel panel1;
        private OxyPlot.WindowsForms.PlotView plotView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ComboBox combo_TimePick;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.TextBox f1_t9;
        private System.Windows.Forms.TextBox f1_t8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox f1_t7;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.TextBox f2_t6;
        private System.Windows.Forms.TextBox f2_t5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox f2_t4;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.TextBox f3_t9;
        private System.Windows.Forms.TextBox f3_t8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox f3_t7;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.TextBox s1_t6;
        private System.Windows.Forms.TextBox s1_t5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox s1_t4;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.TextBox s2_t6;
        private System.Windows.Forms.TextBox s2_t5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox s2_t4;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.TextBox b1_t8;
        private System.Windows.Forms.TextBox b1_t9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox b1_t7;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.TextBox b2_t6;
        private System.Windows.Forms.TextBox b2_t5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox b2_t4;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.TextBox b3_t9;
        private System.Windows.Forms.TextBox b3_t8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox b3_t7;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox f1_t6;
        private System.Windows.Forms.TextBox f1_t5;
        private System.Windows.Forms.TextBox f1_t4;
        private System.Windows.Forms.TextBox f3_t6;
        private System.Windows.Forms.TextBox f3_t5;
        private System.Windows.Forms.TextBox f3_t4;
        private System.Windows.Forms.TextBox b3_t6;
        private System.Windows.Forms.TextBox b3_t5;
        private System.Windows.Forms.TextBox b1_t6;
        private System.Windows.Forms.TextBox b3_t4;
        private System.Windows.Forms.TextBox b1_t5;
        private System.Windows.Forms.TextBox b1_t4;
        private System.Windows.Forms.TextBox b3_t12;
        private System.Windows.Forms.TextBox b3_t11;
        private System.Windows.Forms.TextBox b1_t12;
        private System.Windows.Forms.TextBox b3_t10;
        private System.Windows.Forms.TextBox b1_t11;
        private System.Windows.Forms.TextBox f3_t12;
        private System.Windows.Forms.TextBox b1_t10;
        private System.Windows.Forms.TextBox f1_t12;
        private System.Windows.Forms.TextBox f3_t11;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox f3_t10;
        private System.Windows.Forms.TextBox f1_t11;
        private System.Windows.Forms.TextBox f1_t10;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox TotalTimeTextBox2;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.Button button2;
    }
}